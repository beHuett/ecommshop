package com.cakefactory.catalog;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
class CatalogController {

    private final CatalogService catalogService;

    CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/")
    ModelAndView index() {
        //merely holds both to make it possible for a controller to return both model
        // and view in a single return value
        Map<String, Iterable> myMap = new HashMap<String, Iterable>() ;
        myMap.put("items", this.catalogService.getItems());

        return new ModelAndView("catalog", myMap);
    }
    /*
        @GetMapping("/")
    ModelAndView index() {
        return new ModelAndView("catalog", Map.of("items", this.catalogService.getItems()));
    }
     */

}